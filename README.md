<h3>IMAGE CLASSIFICATION USING CNN</h3>

The task is to classify images into 4 categories using CNN (Convolutional Neural Network). Tensorflow 1.14 has been used. CNN can extract features out of the images on its own and no feature engineering is involved. Also CNN captures spatial interaction.

Images under the following four categories were scraped: -
1. Bed (Train: 83 items, Dev: 28 items, Test: 28 items)
2. Dining set (Train: 75 items, Dev: 25 items, Test: 26 items)
3. Plants and Flowers (Train: 64 items, Dev: 22 items, Test: 22 items)
4. Wardrobe (Train: 92 items, Dev: 31 items, Test: 31 items)
